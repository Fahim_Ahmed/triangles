package
{
	import flash.display.Shape;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	
	import ru.inspirit.utils.Random;

	public class Triangle {
		
		var v0:Vertex;
		var v1:Vertex;
		var v2:Vertex;
		public var lineColor:uint = 0x880066;
		public var lineWidth:Number = 1;
		public var fillAlpha:Number = 1;
		public var fillColor:uint = 0x880066;
		private var s:Shape;		
		private var surface_ambient:uint = 0x888888;
		public static var surface_color:uint = 0x0d559f;
		
		public function Triangle(v0:Vertex, v1:Vertex, v2:Vertex) {
			this.v0 = v0;
			this.v1 = v1;
			this.v2 = v2;
			
			s = new Shape();
		}
		
		public function getTriangleShape():Shape{			
			
			/*var r:uint = Random.integer(0,255);
			var g:uint = Random.integer(150,155);
			var b:uint = Random.integer(180,255);
			
			fillColor = r<<16 | g<<8 | b;*/
			
			s.graphics.clear();
			s.graphics.lineStyle(lineWidth, lineColor);
			s.graphics.beginFill(fillColor, fillAlpha);
			s.graphics.moveTo(v0.x, v0.y);
			s.graphics.lineTo(v1.x, v1.y);
			s.graphics.lineTo(v2.x, v2.y);
			s.graphics.lineTo(v0.x, v0.y);
			s.graphics.endFill();			
			
			return s;
		}
		
		public function isTouched(mx:Number, my:Number):Boolean{
			//trace(mx,v1.x,v2.x);
			return ((mx > v1.x && mx < v2.x) && (my > v0.y && my < v1.y));
			//return true;
		}
		
		public function setColor(col:uint, distance:Number):void{			
			var surface_r = surface_ambient >> 16;
			var surface_g = surface_ambient >> 8 & 0xFF;
			var surface_b = surface_ambient & 0xFF;
						
			var light_r = col >> 16; //light color
			var light_g = col >> 8 & 0xFF;
			var light_b = col & 0xFF;
						
			var ambient_r2 = fillColor >> 16; // current color
			var ambient_g2 = fillColor >> 8 & 0xFF;
			var ambient_b2 = fillColor & 0xFF;
									
			var r3 = light_r * ambient_r2;
			var g3 = light_g * ambient_g2;
			var b3 = light_b * ambient_b2;
			
			if(r3>=255) r3 = 255;
			if(g3>=255) g3 = 255;
			if(b3>=255) b3 = 255;
			
			light_r = light_r + ambient_r2;// + r3;
			light_g = light_g + ambient_g2;// + g3;
			light_b = light_b + ambient_b2;// + b3;
			
			//	light_r *= 0x88/255;
			//light_g *= 0x00/255;
			//light_b *= 0x66/255;			
			
			if(distance>=1) distance = 1;
			//if(distance<=0.1) distance = 0.1;
			light_r *= (1-distance);
			light_g *= (1-distance);
			light_b *= (1-distance);
			
			var sr = surface_color >> 16;
			var sg = surface_color >> 8 & 0xFF;
			var sb = surface_color & 0xFF;
			
			light_r += sr/255 * surface_r;
			light_g += sg/255 * surface_g;
			light_b += sb/255 * surface_b;
			
			if(light_r>=255) light_r = 255;
			if(light_g>=255) light_g = 255;
			if(light_b>=255) light_b = 255;
			
			fillColor = light_r << 16 | light_g << 8 | light_b;
			lineColor = light_r << 16 | light_g << 8 | light_b;
		}
		
		public function setAmbientColor(col:uint):void{
			fillColor = col;
		}
		
		public function get centroid():Point{
			var cx:Number = (1/3)*(v0.x + v1.x + v2.x);
			var cy:Number = (1/3)*(v0.y + v1.y + v2.y);
			
			return new Point(cx,cy);
		}
	}
	
	{
		//-------------------------------------For color 0 - 1--------------------------
		
		/*var surface_ambient = 0x555555; //surface ambient color
		var surface_r = surface_ambient >> 16;
		var surface_g = surface_ambient >> 8 & 0xFF;
		var surface_b = surface_ambient & 0xFF;
		
		surface_r = surface_r/255;
		surface_g = surface_g/255;
		surface_b = surface_b/255;
		
		var light_r = col >> 16; //light color
		var light_g = col >> 8 & 0xFF;
		var light_b = col & 0xFF;
		
		light_r = light_r/255;
		light_g = light_g/255;
		light_b = light_b/255;
		
		var ambient_r2 = fillColor >> 16; // current color
		var ambient_g2 = fillColor >> 8 & 0xFF;
		var ambient_b2 = fillColor & 0xFF;
		
		ambient_r2 = ambient_r2/255;
		ambient_g2 = ambient_g2/255;
		ambient_b2 = ambient_b2/255;
		
		var r3 = light_r * ambient_r2;
		var g3 = light_g * ambient_g2;
		var b3 = light_b * ambient_b2;
		
		if(r3>=1) r3 = 1;
		if(g3>=1) g3 = 1;
		if(b3>=1) b3 = 1;
		
		light_r = light_r + ambient_r2;// + r3;
		light_g = light_g + ambient_g2;// + g3;
		light_b = light_b + ambient_b2;// + b3;
		
		//	light_r *= 0x88/255;
		//light_g *= 0x00/255;
		//light_b *= 0x66/255;			
		
		if(distance>=0.95) distance = 1;
		if(distance<=0.3) distance = 0.3;
		light_r *= (1-distance);
		light_g *= (1-distance);
		light_b *= (1-distance);
		
		light_r += 0x88/255 * surface_r;
		light_g += 0x00/255 * surface_g;
		light_b += 0x66/255 * surface_b;
		
		if(light_r>=1) light_r = 1;
		if(light_g>=1) light_g = 1;
		if(light_b>=1) light_b = 1;			
		
		light_r *= 255;
		light_g *= 255;
		light_b *= 255;*/
	}
}