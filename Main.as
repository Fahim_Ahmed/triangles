﻿package  {	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import net.hires.debug.Stats;
	
	import ru.inspirit.utils.Random;
	import com.bit101.components.ColorChooser;
	
	
	public class Main extends MovieClip {
		
		var triangles:Vector.<Triangle>;
		var vertices:Array;
		
		var sw:Number = stage.stageWidth;
		var sh:Number = stage.stageHeight;
		var segmentsX:uint = 10;
		var segmentsY:uint = 8;
		var segmentW:Number = sw/segmentsX;
		var segmentH:Number = sh/segmentsY;
		var offsetX:Number = 0;
		var offsetY:Number = 0;
		var radius:Number = 500;
		
		var canvas:Bitmap;
		var canvasData:BitmapData;
		
		private var stat:Stats;
		private var temp_index:int;
		private var LIGHT_COLOR:uint = 0x245981;
		private var speed:int = 25;
		private var ease:int = 10;
		private var light:MovieClip;
		private var angle:Number = 0;
		private var perlinData:BitmapData;
		private var dispFilter:DisplacementMapFilter;
		private var pause:Boolean = false;
		private var dbox:ColorChooser;
		private var abox:ColorChooser;
		
		public function Main() {
			trace("constructor code");	
			
			stage.scaleMode = StageScaleMode.NO_BORDER;
			stage.align = StageAlign.TOP_LEFT;
			stage.quality = StageQuality.LOW;
			
			stat = new Stats();
			
			triangles = new Vector.<Triangle>();
			vertices = new Array();
			
			light = new Light();
			light.x = mouseX;
			light.y = mouseY;
			
			canvasData = new BitmapData(sw,sh,true,0xFFFFFF);
			canvas = new Bitmap(canvasData);
			addChild(canvas);
			
			dbox = new ColorChooser(this,stage.stageWidth - 80,15, 0xCC3300);
			dbox.usePopup = true;
			dbox.popupAlign = ColorChooser.BOTTOM;
			abox = new ColorChooser(this,stage.stageWidth - 80,35, 0x000066);
			abox.usePopup = true;
			abox.popupAlign = ColorChooser.BOTTOM;
			
			calculateVertices();
			initialCalculateTriangles();
			drawTriangle();
			
			dispSetup();
			
			canvas.filters = [dispFilter];			
			
			addEventListener(Event.ENTER_FRAME, onTick, false, 0, true);
			stage.addEventListener(KeyboardEvent.KEY_UP, onClickMouse, false, 0, true);
				
			addChild(light);
			addChild(zz);
			addChild(dbox);
			addChild(abox);
			addChild(slider);
			addChild(abt);
			//addChild(stat);			
		}
		
		/*protected function dColor(event:Event):void{
			//LIGHT_COLOR = dbox.value;
		}*/
		
		protected function onClickMouse(event:KeyboardEvent):void
		{
			pause = !pause;
			//dbox.visible = !pause;
			//abox.visible = !pause;
		}
		protected function onTick(event:Event):void
		{
			
			
			LIGHT_COLOR = dbox.value;
			Triangle.surface_color = abox.value;
			
			dispFilter.scaleX = slider.slide.value;
			dispFilter.scaleY = slider.slide.value;
			canvas.filters = [dispFilter];
				
			vertices = [];
			calculateVertices();			
			recalculateTriangles();
			
			if(!pause) lightAnimate();
			
			calcDistance(light.x, light.y, radius);
			
			//calcDistance(mouseX, mouseY, radius);
			//calcDistance(550, 300, radius);
			drawTriangle();
			
		}
		
		private function drawTriangle():void
		{
			canvasData.fillRect(new Rectangle(0,0,sw,sh), 0xFF555555);
			
			for(var i=triangles.length-1; i>=0; i--){				
				canvasData.draw(triangles[i].getTriangleShape());//,null,null,BlendMode.ADD);				
			}			
		}
		
		private function calcDistance(mx:Number, my:Number, r:Number):void
		{
			//-------------------------------------Full range
			for(var i = triangles.length-1; i>=0; i--){
				var t:Triangle = triangles[i];
				
				var tx = t.centroid.x;
				var ty = t.centroid.y;
				
				var dist = Math.sqrt((tx-mx)*(tx-mx) + (ty-my)*(ty-my));				
				t.setColor(LIGHT_COLOR, dist/(r));
			}			
			
			//--------------------------------------range limit
			/*var loc_lx = mx-r;
			if(loc_lx<0) loc_lx = 0;
			//var loc_ly = my;
			
			//var loc_tx = mx;
			var loc_ty = my - r;
			if(loc_ty<0) loc_ty = 0;

			var loc_rx = mx + r;
			if(loc_rx > sw) loc_rx = sw-1;
			//var loc_ry = my;
			
			//var loc_bx = mx;
			var loc_by = my + r;
			if(loc_by > sh) loc_by = sh-1;
			
			var index_tl = locationCalculate(loc_lx,loc_ty);
			var index_br = locationCalculate(loc_rx,loc_by);
			
			//trace(index_tl, index_br,loc_rx,loc_by);
			
			for(var i = index_tl; i<=index_br+1; i++){
				var t:Triangle = triangles[i];
				
				var tx = t.centroid.x;
				var ty = t.centroid.y;
				
				var dist = Math.sqrt((tx-mouseX)*(tx-mouseX) + (ty-mouseY)*(ty-mouseY));
				
				if(dist < r){
					t.setColor(uint("0xFF8800"), (dist/r));					
				}
			}*/			
		}
		
		private function locationCalculate(mouseX:Number, mouseY:Number):int
		{
			var col:int = mouseX/segmentW;
			var row:int = mouseY/segmentH;			
			
			var index:uint = col * segmentsY * 2 + row * 2;
			if(index == triangles.length) index--;
			
			return index;
		}
		
		private function checkBounds(index:uint):void
		{
			/*for(var i=triangles.length-1; i>=0; i--){				
				if(triangles[i].isTouched(mouseX, mouseY)){					
					triangles[i].setColor(uint("0xFF8800"));	
					return;
				}
			}*/
			
			var tx = triangles[index].centroid.x;
			var ty = triangles[index].centroid.y;
			
			var dist0 = Math.sqrt((tx-mouseX)*(tx-mouseX) + (ty-mouseY)*(ty-mouseY));
			
			tx = triangles[index+1].centroid.x;
			ty = triangles[index+1].centroid.y;
			var dist1 = Math.sqrt((tx-mouseX)*(tx-mouseX) + (ty-mouseY)*(ty-mouseY));
			
			if(dist0 < dist1){
				triangles[index].setColor(uint("0xFF8800"), 1);				
			}else{
				triangles[index+1].setColor(uint("0xFF8800"), 1);
			}
		}
		
		private function calculateVertices():void
		{
			for(var i=0; i<=segmentsX; i++){				
				vertices.push(new Array());				
				for(var j=0; j<=segmentsY; j++){
					var vertex:Vertex = new Vertex(offsetX + i*segmentW, offsetY + j*segmentH);
					vertices[i].push(vertex);
					//trace(vertices.length, vertices[i].length);
				}
			}
		}
		
		private function recalculateTriangles():void
		{
			var tmp:Array = new Array();	
			for(var i=0; i<segmentsX; i++){
				//var axis:Array = vertices[i];
				for(var j=0; j<segmentsY; j++){
					var v0:Vertex = vertices[i+0][j+0];					
					var v1:Vertex = vertices[i+0][j+1];					
					var v2:Vertex = vertices[i+1][j+0];					
					var v3:Vertex = vertices[i+1][j+1];
					
					var t0:Triangle = new Triangle(v0,v1,v2);
					var t1:Triangle = new Triangle(v2,v1,v3);
					tmp.push(t0,t1);					
				}
			}
			
			for(var _i=triangles.length-1; _i >= 0; _i--){
				triangles[_i].v0 = tmp[_i].v0;
				triangles[_i].v1 = tmp[_i].v1;
				triangles[_i].v2 = tmp[_i].v2;
			}
			tmp = [];
		}
		
		private function initialCalculateTriangles():void
		{			
			for(var i=0; i<segmentsX; i++){
				//var axis:Array = vertices[i];
				for(var j=0; j<segmentsY; j++){
					var v0:Vertex = vertices[i+0][j+0];					
					var v1:Vertex = vertices[i+0][j+1];					
					var v2:Vertex = vertices[i+1][j+0];					
					var v3:Vertex = vertices[i+1][j+1];
					
					var t0:Triangle = new Triangle(v0,v1,v2);
					var t1:Triangle = new Triangle(v2,v1,v3);
					triangles.push(t0,t1);
					//trace(i,j);
				}
			}
		}
		
		private function lightAnimate():void{
			var targetX:int = mouseX - light.x;
			var targetY:int = mouseY - light.y;
			var rotation:int = Math.atan2(targetY, targetX) * 180 / Math.PI;
			if (Math.abs(rotation - light.rotation) > 180)
			{
				if (rotation > 0 && light.rotation < 0)
					light.rotation -= (360 - rotation + light.rotation) / ease;
				else if (light.rotation > 0 && rotation < 0)
					light.rotation += (360 - rotation + light.rotation) / ease;
			}
			else if (rotation < light.rotation)
				light.rotation -= Math.abs(light.rotation - rotation) / ease;
			else
				light.rotation += Math.abs(rotation - light.rotation) / ease;
			
			var vx:Number = speed * (90 - Math.abs(light.rotation)) / 90;
			var vy:Number;
			if (light.rotation < 0)
				vy = -speed + Math.abs(vx);
			else
				vy = speed - Math.abs(vx);
			
			light.x += vx;
			light.y += vy;
			
//			var rad:Number = angle * (Math.PI / 180); // Converting Degrees To Radians
//			light.x = (mouseX + 30 * Math.cos(rad));
//			light.y = mouseY + 30 * Math.sin(rad);
//			angle+=speed;
		}
		
		private function dispSetup():void
		{
			perlinData =  new BitmapData(sw,sh, true, 0x00FFFFFF);			
			var seed:Number = Math.floor(Math.random() * 10000); 
			var channels:uint = BitmapDataChannel.BLUE;			
			var numOctaves:int=2;
			
			perlinData.perlinNoise(100, 100, numOctaves, seed, false, false, channels, true, null);
			
			dispFilter = new DisplacementMapFilter();			
			dispFilter.mapBitmap=perlinData;			
			dispFilter.componentX=BitmapDataChannel.RED;			
			dispFilter.componentY=BitmapDataChannel.RED;			
			dispFilter.scaleX=150;			
			dispFilter.scaleY=150;			
			dispFilter.mode=DisplacementMapFilterMode.CLAMP;
		}
	}	
}
