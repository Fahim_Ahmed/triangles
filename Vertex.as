﻿package
{
	public class Vertex	{
		
		private var _x:Number;
		private var _y:Number;
		
		public function Vertex(x:Number = 0, y:Number = 0) {
			this._x = x;
			this._y = y;			
		}

		public function get x():Number
		{
			return _x;
		}

		public function set x(value:Number):void
		{
			_x = value;
		}

		public function get y():Number
		{
			return _y;
		}

		public function set y(value:Number):void
		{
			_y = value;
		}

	}
}